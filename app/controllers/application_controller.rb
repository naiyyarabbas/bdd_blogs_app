class ApplicationController < ActionController::Base

	rescue_from ActiveRecord::RecordNotFound, with: :no_resource_found

	protected
	def no_resource_found
	end
end
