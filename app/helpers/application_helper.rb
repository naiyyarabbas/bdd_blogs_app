module ApplicationHelper
	def flash_class(flash_type)
    case flash_type.to_sym
      when :notice, :success  then 'alert-success'
      when :info    then 'alert-info'
      when :error   then 'alert-danger'
      when :warning then 'alert-warning'
    end
  end
end
