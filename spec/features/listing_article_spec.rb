require 'rails_helper'

RSpec.feature "Listing Article:" do
	before do 
		@user = User.create(email: 'john@example.com', password: 'password')
		@article1 = Article.create(title: 'Test Article 1', body: 'Lorem ipsum dollar sit amet', user: @user)
		@article2 = Article.create(title: 'Test Article 2', body: 'Lorem ipsum dollar sit amet. Lorem ipsum dollar sit amet', user: @user)
	end

	scenario 'User listing articles' do
		visit '/'

		expect(page).to have_content(@article1.title)
		expect(page).to have_content(@article1.body)
		expect(page).to have_content(@article2.title)
		expect(page).to have_content(@article2.body)
		expect(page).to have_link(@article1.title)
		expect(page).to have_link(@article2.title)
	end

	scenario 'With articles created and User is not signed in' do
		visit '/'

		expect(page).to have_content(@article1.title)
		expect(page).to have_content(@article1.body)
		expect(page).to have_content(@article2.title)
		expect(page).to have_content(@article2.body)
		expect(page).to have_link(@article1.title)
		expect(page).to have_link(@article2.title)
		expect(page).not_to have_link('New Article')
	end

	scenario 'With articles created and User is signed in' do
		login_as(@user)
		visit '/'

		expect(page).to have_content(@article1.title)
		expect(page).to have_content(@article1.body)
		expect(page).to have_content(@article2.title)
		expect(page).to have_content(@article2.body)
		expect(page).to have_link(@article1.title)
		expect(page).to have_link(@article2.title)
		expect(page).to have_link('New Article')
	end

	scenario 'User listing no articles' do
		Article.delete_all
		visit '/'

		expect(page).not_to have_content(@article1.title)
		expect(page).not_to have_content(@article1.body)
		expect(page).not_to have_content(@article2.title)
		expect(page).not_to have_content(@article2.body)
		expect(page).not_to have_link(@article1.title)
		expect(page).not_to have_link(@article2.title)

		within('h1#no-articles') do
			expect(page).to have_content('No articles created')
		end
	end
end