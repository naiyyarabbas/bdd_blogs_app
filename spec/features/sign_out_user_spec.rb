require 'rails_helper'

RSpec.feature 'User sign out' do
	before do
		@user = User.create!(email: 'john@example.com', password: 'password')
		visit '/'
		click_link 'Sign in'
		fill_in 'user[email]', with: @user.email
		fill_in 'user[password]', with: @user.password
		click_button 'Log in'
	end

	scenario '' do
		visit '/'
		click_link 'Sign out'
		expect(page).to have_content('Signed out successfully')
		expect(page).not_to have_link('Signed out')
	end
end