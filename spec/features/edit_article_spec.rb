require 'rails_helper'

RSpec.feature 'Editing article' do
	before do
		user = User.create(email: 'john@example.com', password: 'password')
		login_as(user)
		@article = Article.create(title: 'Edit article', body: 'Test test test test', user: user)
	end
	
	scenario 'User updates an article' do
		visit '/'
		click_link @article.title
		click_link 'Edit Article'

		fill_in 'article[title]', with: 'Updated Article'
		fill_in 'article[body]', with: 'Updated body'

		click_button 'Update Article'

		expect(page).to have_content("Article has been updated")
		expect(page.current_path).to eq(article_path(@article))
	end

	scenario 'User Fails to updates an article' do
		visit '/'
		click_link @article.title
		click_link 'Edit Article'

		fill_in 'article[title]', with: ''
		fill_in 'article[body]', with: 'Updated body of article'

		click_button 'Update Article'

		expect(page).to have_content("Article has not been updated")
		expect(page.current_path).to eq(article_path(@article))
	end
end