require 'rails_helper'

RSpec.feature 'Deleting article' do
	before do
		@user = User.create!(email: 'john@example.com', password: 'password')
		login_as(@user)
		@article = Article.create(title: 'Article to delete', body: 'Lorem ipsum delete', user: @user)
	end
	
	scenario 'User deletes an article' do
		visit '/'
		click_link @article.title
		click_link 'Delete Article'

		expect(page).to have_content("Article has been deleted")
		expect(page.current_path).to eq(articles_path)
	end
end