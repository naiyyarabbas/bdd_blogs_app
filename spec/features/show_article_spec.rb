require 'rails_helper'

RSpec.feature "Show Article:" do
	before do 
		@user = User.create!(email: 'john@example.com', password: 'password')
		@user2 = User.create!(email: 'doe@example.com', 	password: 'password')
		@article = Article.create(title: 'Test Article 1', body: 'Lorem ipsum dollar sit amet', user: @user)
	end

	scenario 'User shows an article' do
		visit '/'
		click_link @article.title
		expect(page).to have_content(@article.title)
		expect(page).to have_content(@article.body)
		expect(current_path).to eq(article_path(@article))
	end

	scenario 'User is signed in and user is an owner of article show Edit/Delete links' do
		login_as(@user)
		visit '/'
		click_link @article.title
		expect(page).to have_content(@article.title)
		expect(page).to have_content(@article.body)
		expect(current_path).to eq(article_path(@article))
		expect(page).to have_link('Edit Article')
		expect(page).to have_link('Delete Article')
	end

	scenario 'User is signed in and user is not an owner of article hide Edit/delete' do
		login_as(@user2)
		visit '/'
		click_link @article.title
		expect(page).to have_content(@article.title)
		expect(page).to have_content(@article.body)
		expect(current_path).to eq(article_path(@article))
		expect(page).not_to have_link('Edit Article')
		expect(page).not_to have_link('Delete Article')
		
	end

	scenario 'to a non-signed in user hides Edit/Delete Links' do
		visit '/'
		click_link @article.title
		expect(page).to have_content(@article.title)
		expect(page).to have_content(@article.body)
		expect(current_path).to eq(article_path(@article))
		expect(page).not_to have_link('Edit Article')
		expect(page).not_to have_link('Delete Article')
	end
end