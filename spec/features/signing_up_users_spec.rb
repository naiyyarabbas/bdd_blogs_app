require 'rails_helper'

RSpec.feature 'Sign up User' do
	scenario 'with valid credentials' do
		visit '/'
		click_link 'Sign up'

		fill_in 'user[email]', with: 'user@example.com'
		fill_in 'user[password]', with: '1qaz2wsx'
		fill_in 'user[password_confirmation]', with: '1qaz2wsx'
		click_button 'Sign up'

		expect(page).to have_content('You have signed up successfully')
	end

	scenario 'with invalid credentials' do
		visit '/'
		click_link 'Sign up'

		fill_in 'user[email]', with: ''
		fill_in 'user[password]', with: ''
		fill_in 'user[password_confirmation]', with: ''
		click_button 'Sign up'

		# expect(page).to have_content('You have not signed up successfully')
	end
end