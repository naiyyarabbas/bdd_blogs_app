require 'rails_helper'

RSpec.feature 'User signing in' do
	before do
		@user = User.create!(email: 'john@example.com', password: 'password')
	end

	scenario 'With valid credentials' do
		visit '/'
		click_link 'Sign in'
		fill_in 'user[email]', with: @user.email
		fill_in 'user[password]', with: @user.password
		click_button 'Log in'

		expect(page).to have_content('Signed in successfully')
		expect(page).to have_link('Sign out')
		expect(page).not_to have_link('Sign in')
		expect(page).not_to have_link('Sign up')
	end

	scenario 'With invalid credentials' do
		visit '/'
		click_link 'Sign in'
		fill_in 'user[email]', with: ''
		fill_in 'user[password]', with: ''
		click_button 'Log in'

		expect(page).to have_content('Invalid Email or password')
		expect(page).to have_link('Sign in')
		expect(page).to have_link('Sign up')
		expect(page.current_path).to eq(new_user_session_path)
	end
end