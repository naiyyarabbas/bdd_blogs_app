require 'rails_helper'

RSpec.describe "Articles", type: :request do
	before do
		@user = User.create(email: 'john@example.com', password: 'password')
		@user2 = User.create(email: 'doe@example.com', password: 'password')
		@article = Article.create!(title: "Title one", body: "Body of article one", user: @user)
	end

	describe 'GET: /articles/:id/edit' do
		context 'With not signed in user' do
			before{get "/articles/#{@article.id}/edit"}
			it 'redirect to the signed in page' do
				expect(response.status).to eq 302
				flash_message = 'You need to sign in or sign up before continuing.'
				expect(flash[:alert]).to eq flash_message
			end
		end

		context 'Signed in user who is non owner' do
			before do
				login_as(@user2)
				get "/articles/#{@article.id}/edit"
			end

			it 'Redirect to home page' do
				expect(response.status).to eq 302
				flash_message = 'You can only edit your own article.'
				expect(flash[:alert]).to eq flash_message
			end
		end
	end

	describe 'GET: /aricles/:id' do
		context 'With existing article' do
			before{get "/articles/#{@article.id}"}
			it 'Handles existing article' do
				expect(response.status).to eq 200
			end
		end
		
		context 'With non existing article' do
			before{get "/articles/100"}
			it 'Handles Non existing article' do
				expect(response.status).to eq 302
				flash_message = 'No article found'
				expect(flash[:error]).to eq(flash_message)
			end
		end
	end
end
